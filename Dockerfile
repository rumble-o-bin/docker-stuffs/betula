FROM golang:1.22-alpine as build

ENV BETULA_VERSION v1.3.1

RUN apk add build-base \
    && CGO_ENABLED=1 go install git.sr.ht/~bouncepaw/betula/cmd/betula@${BETULA_VERSION}

FROM alpine:3.19

COPY --from=build /go/bin/betula /usr/local/bin/betula

ENTRYPOINT [ "/usr/local/bin/betula", "-port", "8080", "/srv/betula/bookmarks.betula"]
